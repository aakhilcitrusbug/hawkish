/* Set the width of the side navigation to 250px */

function openNav() {
  document.getElementById("mySidenav").style.width = "70%";
  document.getElementById("nav-res").style.opacity = "1";
  document.getElementById("cd-shadow-layer").style.display = "block";
  
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("cd-shadow-layer").style.display = "none";  
  //$('#nav-res').hide().fadeIn('slow');
} 

$(document).ready(function(){ 
  $("#cd-shadow-layer").click(function(){
    $("#cd-shadow-layer").addClass("display-none");

    closeNav(); 
  });
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});


$(document).ready(function(){
  $(".user-row1").click(function(){
    $(".left-4").addClass("left-4none");
    $(".left-8").addClass("left-8show");
  });
  $(".backbtn").click(function(){
    $(".left-4").removeClass("left-4none");
    $(".left-8").removeClass("left-8show");
  });
});

function show1(){
  document.getElementById('box1').style.display = 'block';
  document.getElementById('box2').style.display ='none';
}
function show2(){
  document.getElementById('box2').style.display = 'block';
  document.getElementById('box1').style.display ='none';
}

$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:7000,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:1
      },
      1000:{
        items:1
      }
    }
  })

  $('#owl-demo2').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:4000,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:2
      },
      1000:{
        items:3
      }
    }
  })

  });
